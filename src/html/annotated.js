var annotated =
[
    [ "description", "namespacedescription.html", "namespacedescription" ],
    [ "frames", "namespaceframes.html", "namespaceframes" ],
    [ "keccak", "namespacekeccak.html", "namespacekeccak" ],
    [ "keccakConstants", "namespacekeccak_constants.html", "namespacekeccak_constants" ],
    [ "keccakUtils", "namespacekeccak_utils.html", "namespacekeccak_utils" ],
    [ "panels", "namespacepanels.html", "namespacepanels" ],
    [ "presentationData", "namespacepresentation_data.html", "namespacepresentation_data" ],
    [ "TestScript", "class_test_script.html", "class_test_script" ]
];