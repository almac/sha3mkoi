var searchData=
[
  ['setabsorbciondata',['setAbsorbcionData',['../classpresentation_data_1_1_presentation_data.html#a6b03d307bdaefce1d30b805d354ac7a4',1,'presentationData::PresentationData']]],
  ['setdatachi',['setDataChi',['../classpresentation_data_1_1_presentation_data.html#a0e0a2543d838635f9061b255d18cd1b9',1,'presentationData::PresentationData']]],
  ['setdataiota',['setDataIota',['../classpresentation_data_1_1_presentation_data.html#aeeb4d8404963cc0202a33136c49edbb7',1,'presentationData::PresentationData']]],
  ['setdatarhopi',['setDataRhoPi',['../classpresentation_data_1_1_presentation_data.html#a91f03920aacb24bf5ecb6ea099fb34ee',1,'presentationData::PresentationData']]],
  ['setdatasquize',['setDataSquize',['../classpresentation_data_1_1_presentation_data.html#afb96a1cbe22de8a69cca99a3ee600375',1,'presentationData::PresentationData']]],
  ['setdatatheta',['setDataTheta',['../classpresentation_data_1_1_presentation_data.html#afae7dbe77c0689525ea4f028365910e1',1,'presentationData::PresentationData']]],
  ['setinitialisationdata',['setInitialisationData',['../classpresentation_data_1_1_presentation_data.html#ad81a31fd390a25f4e57969bb1add62c4',1,'presentationData::PresentationData']]],
  ['sha3start',['Sha3Start',['../classframes_1_1_sha3_start.html#a8d73513b2f7d9a2368074c51a1c2d71e',1,'frames::Sha3Start']]]
];
