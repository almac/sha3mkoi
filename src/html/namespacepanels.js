var namespacepanels =
[
    [ "FinalPanel", "classpanels_1_1_final_panel.html", "classpanels_1_1_final_panel" ],
    [ "GenericRoundFunctionDataPanel", "classpanels_1_1_generic_round_function_data_panel.html", "classpanels_1_1_generic_round_function_data_panel" ],
    [ "GenericRoundFunctionDescriptionPanel", "classpanels_1_1_generic_round_function_description_panel.html", "classpanels_1_1_generic_round_function_description_panel" ],
    [ "GenericRoundFunctionPanel", "classpanels_1_1_generic_round_function_panel.html", "classpanels_1_1_generic_round_function_panel" ],
    [ "InitialPanel", "classpanels_1_1_initial_panel.html", "classpanels_1_1_initial_panel" ]
];