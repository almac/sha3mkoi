package presentationData;

import java.math.BigInteger;
import java.util.ArrayList;

import keccakUtils.KeccakUtils;

/**
 * Funkcja przetrzymujaca dane do wyswietlenia w panelach przedstawiajacych
 * kolejne kroki dzialania algorytmu oblicznaia skotu funkjci SHA-3
 */
public class PresentationData {

	/**
	 * Skrot wiadomosci
	 */
	public static String hashedMessage = "";
	/**
	 * Wiadomosc poddana operacjom przeksztalcenia w wynik funkcji skrotu
	 */
	public static String message = "";
	/**
	 * Opis stanu przed etapem uzupelniania
	 */
	public static String messageInit = "";
	/**
	 * Opis stanu po etapie uzupelniania
	 */
	public static String messageAfterPadding = "";
	/**
	 * Opis stanu przed etapem absorbcji
	 */
	public static String stateBeforeAbsorbcion = "";
	/**
	 * Opis stanu przed etapem absorbcji
	 */
	public static String stateAfterAbsorbcion = "";

	/**
	 * Ustawienie czy pobierac stan kroku Theta
	 */
	public static boolean setDataThetaStep = true;
	/**
	 * Opis stanu przed krokiem Theta
	 */
	public static String stateBeforeTheta = "";
	/**
	 * Opis stanu po kroku Theta
	 */
	public static String stateAfterTheta = "";
	/**
	 * Stan przed krokiem Theta
	 */
	public static ArrayList<String> beforeTheta = new ArrayList<String>();
	/**
	 * Stan po kroku Theta
	 */
	public static ArrayList<String> afterTheta = new ArrayList<String>();

	/**
	 * Ustawienie czy pobierac stan kroku Rho/Pi
	 */
	public static boolean setDataRhoPiStep = true;
	/**
	 * Opis stanu przed krokiem Rho/Pi
	 */
	public static String stateBeforeRhoPi = "";
	/**
	 * Opis stanu po kroku Rho/Pi
	 */
	public static String stateAfterRhoPi = "";
	/**
	 * Stan przed krokiem Rho/Pi
	 */
	public static BigInteger[][] beforeRhoPi = new BigInteger[5][5];
	/**
	 * Stan po kroku Rho/Pi
	 */
	public static BigInteger[][] afterRhoPi = new BigInteger[5][5];

	/**
	 * Ustawienie czy pobierac stan kroku Chi
	 */
	public static boolean setDataChiStep = true;
	/**
	 * Opis stanu przed krokiem Chi
	 */
	public static String stateBeforeChi = "";
	/**
	 * Opis stanu po kroku Chi
	 */
	public static String stateAfterChi = "";
	/**
	 * Stan przed krokiem Chi
	 */
	public static ArrayList<String> beforeChi = new ArrayList<String>();
	/**
	 * Stan po kroku Chi
	 */
	public static ArrayList<String> afterChi = new ArrayList<String>();

	/**
	 * Ustawienie czy pobierac stan kroku Iota
	 */
	public static boolean setDataIotaStep = true;
	/**
	 * Opis stanu przed krokiem Iota
	 */
	public static String stateBeforeIota = "";
	/**
	 * Opis stanu po kroku Iota
	 */
	public static String stateAfterIota = "";
	/**
	 * Stan przed krokiem Iota
	 */
	public static ArrayList<String> beforeIota = new ArrayList<String>();
	/**
	 * Stan po kroku Iota
	 */
	public static BigInteger[][] afterIota = new BigInteger[5][5];

	/**
	 * Liczba 'wycisniec' gabki funkcji haszujacej SHA-3
	 */
	public static int squizesToMake = 3;

	/**
	 * Kolejne etapy tworzenie sie wyniku funkcji skrotu SHA-3
	 */
	public static ArrayList<String> squizesArray = new ArrayList<String>();

	/**
	 * Opis wyniku funkcji haszujacej na poczatku wyciskania
	 */
	public static String hashInitial = "";

	/**
	 * Opis wyniku funkcji haszujacej po kilku krokach wyciskania
	 */
	public static String hashAfterSquize = "";
	/**
	 * Dane stanu S uzywane do wyciskania skrotu wiadomosci
	 */
	public static ArrayList<String> squizesStateArray = new ArrayList<String>();

	/**
	 * Funkcja czyszczaca dane z poprzedniego kroku
	 */
	public static void resetData() {
		hashedMessage = "";
		message = "";
		messageInit = "";
		messageAfterPadding = "";
		stateBeforeAbsorbcion = "";
		stateAfterAbsorbcion = "";
		setDataThetaStep = true;
		stateBeforeTheta = "";
		stateAfterTheta = "";
		beforeTheta = new ArrayList<String>();
		afterTheta = new ArrayList<String>();
		setDataRhoPiStep = true;
		stateBeforeRhoPi = "";
		stateAfterRhoPi = "";
		beforeRhoPi = new BigInteger[5][5];
		afterRhoPi = new BigInteger[5][5];
		setDataChiStep = true;
		stateBeforeChi = "";
		stateAfterChi = "";
		beforeChi = new ArrayList<String>();
		afterChi = new ArrayList<String>();
		setDataIotaStep = true;
		stateBeforeIota = "";
		stateAfterIota = "";
		beforeIota = new ArrayList<String>();
		afterIota = new BigInteger[5][5];
		squizesToMake = 3;
		squizesArray = new ArrayList<String>();
		hashInitial = "";
		hashAfterSquize = "";
		squizesStateArray = new ArrayList<String>();
	}

	/**
	 * Funkcja ustawiajaca dane po krokach wyciskania
	 */
	public static void setDataSquize() {

		StringBuilder sb1 = new StringBuilder();
		sb1.append("Z = \"\" \n\n");
		for (int i = 0; i < squizesArray.size(); i++) {
			sb1.append("Dane stanu S u�yte do " + (i + 1) + " iteracji:\n");
			sb1.append("S = " + squizesStateArray.get(i) + "\n\n");
		}

		hashInitial = sb1.toString();

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < squizesArray.size(); i++) {
			sb.append("Wynik funkcji skrotu po " + (i + 1) + " iteracji:\n");
			sb.append("Z = " + squizesArray.get(i) + "\n\n");
		}
		hashAfterSquize = sb.toString();

	}

	/**
	 * Funkcja ustawiajaca dane po kroku Iota
	 */
	public static void setDataIota() {
		StringBuilder sb = new StringBuilder();

		sb.append(beforeIota.get(0)).append("\n").append("\n");
		sb.append("Stala rundowa uzyta do operacji XOR:").append("\n").append(beforeIota.get(1));

		stateBeforeIota = sb.toString();

		StringBuilder sb1 = new StringBuilder();
		sb1.append(stateToString("S", afterIota));
		stateAfterIota = sb1.toString();
	}

	/**
	 * Funkcja ustawiajaca dane po kroku Chi
	 */
	public static void setDataChi() {
		StringBuilder sb = new StringBuilder();
		for (String s : beforeChi) {
			sb.append(s).append("\n").append("\n");
		}
		stateBeforeChi = sb.toString();

		StringBuilder sb1 = new StringBuilder();
		for (String s : afterChi) {
			sb1.append(s).append("\n").append("\n");
		}
		stateAfterChi = sb1.toString();
	}

	/**
	 * Funkcja ustawiajaca dane po kroku Rho/Pi
	 */
	public static void setDataRhoPi() {
		StringBuilder sb = new StringBuilder();
		sb.append(stateBToString(beforeRhoPi));
		stateBeforeRhoPi = sb.toString();

		StringBuilder sb1 = new StringBuilder();
		sb1.append(stateBToString(afterRhoPi));
		stateAfterRhoPi = sb1.toString();
	}

	/**
	 * Funkcja ustawiajaca dane po kroku Theta
	 */
	public static void setDataTheta() {
		StringBuilder sb = new StringBuilder();
		for (String s : beforeTheta) {
			sb.append(s).append("\n").append("\n");
		}

		stateBeforeTheta = sb.toString();

		StringBuilder sb1 = new StringBuilder();
		for (String s : afterTheta) {
			sb1.append(s).append("\n").append("\n");
		}
		stateAfterTheta = sb1.toString();
	}

	/**
	 * Funkcja ustawiajaca dane do wizualizacji kroku absorbcji
	 * 
	 * @param stateZero
	 *            stan poczatkowy
	 * @param Pi
	 *            blok Pi wiadomosci P
	 * @param stateAbsorbcion
	 *            stan po kroku absorbcji
	 */
	public static void setAbsorbcionData(BigInteger[][] stateZero, ArrayList<BigInteger> Pi,
			BigInteger[][] stateAbsorbcion) {

		StringBuilder sb = new StringBuilder();
		sb.append("Stan poczatkowy:\n");
		sb.append(stateToString("S", stateZero)).append("\n").append("\n");
		sb.append("Blok Pi wiadomosci P uzyty do operacji XOR:").append("\n");
		sb.append("Pi=[");
		for (BigInteger i : Pi) {
			sb.append("\n").append(KeccakUtils.bigIntegerToHex(i)).append(",");
		}
		sb.setLength(sb.length() - 1);
		sb.append("]");
		stateAfterAbsorbcion = sb.toString();

		StringBuilder sb1 = new StringBuilder();
		sb1.append("Stan po XOR z jednym blokiem wiadomosci Pi:\n");
		sb1.append(stateToString("S", stateAbsorbcion));
		stateBeforeAbsorbcion = sb1.toString();

	}

	/**
	 * Funkcja zapisujaca stan tablicy BigInteger[][] na tekst zapisany w
	 * systemie heksadecymalnym
	 * 
	 * @param name
	 *            nazwa stanu
	 * @param state
	 *            stan do zapisania
	 * @return tekst stworzony z tablicy
	 */
	public static String stateToString(String name, BigInteger[][] state) {
		StringBuilder sb = new StringBuilder();
		sb.append(name).append("=[");

		for (int i = 0; i < state[0].length; i++) {
			for (int j = 0; j < state[0].length; j++) {
				sb.append("\n").append(KeccakUtils.bigIntegerToHex(state[i][j])).append(",");
			}
		}
		sb.setLength(sb.length() - 1);
		sb.append("]");
		return sb.toString();
	}

	/**
	 * Funkcja zapisujaca stan tablicy BigInteger[] na tekst zapisany w systemie
	 * heksadecymalnym
	 * 
	 * @param name
	 *            nazwa stanu
	 * @param state
	 *            stan do zapisania
	 * @return tekst stworzony z tablicy
	 */
	public static String stateToString(String name, BigInteger[] state) {
		StringBuilder sb = new StringBuilder();
		sb.append(name).append("=[");

		for (int i = 0; i < state.length; i++) {
			sb.append("\n").append(KeccakUtils.bigIntegerToHex(state[i])).append(",");
		}
		sb.setLength(sb.length() - 1);
		sb.append("]");
		return sb.toString();
	}

	/**
	 * Funkcja zapisujaca tablice B na tekst zapisany w systemie heksadecymalnym
	 * 
	 * @param state
	 *            tablica do zapisania
	 * @return tekst stworzony z tablicy
	 */
	private static String stateBToString(BigInteger[][] state) {
		StringBuilder sb = new StringBuilder();
		sb.append("B=[");

		for (int i = 0; i < state[0].length; i++) {
			for (int j = 0; j < state[0].length; j++) {
				sb.append("\n").append(KeccakUtils.bigIntegerToHex(state[i][j])).append(",");
			}
		}
		sb.setLength(sb.length() - 1);
		sb.append("]");
		return sb.toString();
	}

	/**
	 * Funkcja ustawiajaca dane do wizualizacji kroku inicjalizacji
	 * 
	 * @param messageHexed
	 *            wiadomosc zapisana w systemie heksadecymalny
	 * @param paddedMessage
	 *            uzupelniona wiadomosc
	 */
	public static void setInitialisationData(String messageHexed, String paddedMessage) {

		String brokenMessage = breakString(paddedMessage, 40);

		StringBuilder sb = new StringBuilder();
		sb.append("Wiadomosc wejsciowa:\n");
		sb.append(message);
		messageInit = sb.toString();

		sb = new StringBuilder();
		sb.append("Wiadomosc zapisana w systemie heksadecymalnym:\n");
		sb.append(messageHexed + "\n\n");
		sb.append("Wiadomosc uzupelniona sposobem pad10*1:\n");
		sb.append(brokenMessage + "\n");
		messageAfterPadding = sb.toString();
	}

	/**
	 * Funkcja lamiaca ciag znakow
	 * 
	 * @param string
	 *            wiadomosc do podzielenia
	 * @param breakPoint
	 *            znak po ktorym nastepuje ciecie
	 * @return podzielony ciag znakow
	 */
	private static String breakString(String string, int breakPoint) {
		StringBuilder sbMessage = new StringBuilder();

		for (int i = 0; i < string.length(); i++) {
			sbMessage.append(string.charAt(i));
			if (i % breakPoint == 0 && i != 0) {
				sbMessage.append("\n");
			}
		}

		return sbMessage.toString();
	}

	/**
	 * Funkcja kopiujaca tablice
	 * 
	 * @param src
	 *            tablica zrodlowa
	 * @param dest
	 *            tablica docelowa
	 * @return skopiowana tablica
	 */
	public static BigInteger[][] copyArray(BigInteger[][] src, BigInteger[][] dest) {
		for (int i = 0; i < src.length; i++) {
			System.arraycopy(src[i], 0, dest[i], 0, src[i].length);
		}
		return dest;
	}

	/**
	 * Funkcja kopiujaca tablice
	 * 
	 * @param src
	 *            tablica zrodlowa
	 * @param dest
	 *            tablica docelowa
	 * @return skopiowana tablica
	 */
	public static BigInteger[] copyArray(BigInteger[] src, BigInteger[] dest) {
		System.arraycopy(src, 0, dest, 0, src.length);
		return dest;
	}

}
