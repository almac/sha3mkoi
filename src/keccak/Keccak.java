package keccak;

import java.math.BigInteger;
import java.util.ArrayList;

import keccakConstants.Offset;
import keccakConstants.RC;
import keccakUtils.KeccakUtils;
import presentationData.PresentationData;

/**
 * Klasa odpowiedzialna za przetworzenie zadanej wiadomosci na wynik funkcji
 * skrotu SHA-3 Keccak
 * 
 * @author Maciej Aleszczyk
 * @author Grzegorz Dziubka
 */
public class Keccak {

	/**
	 * Dwuwymiarowa tablica obrazujaca stan danych uzywanych w funkcji skrotu
	 * SHA-3
	 */
	private BigInteger[][] S;

	/**
	 * Dwuwymiarowa tablica B uzywana w funkcji rundowej w krokach Ro,Pi,Chi
	 */
	private BigInteger[][] B;

	/**
	 * Jednowymiarowa tablica C uzywana w funkcji runodwej w kroku theta
	 */
	private BigInteger[] C;

	/**
	 * Jednowymiarowa tablica C uzywana w funkcji runodwej w kroku theta
	 */
	private BigInteger[] D;

	/**
	 * Dlugosc bloku funkcji rundowej SHA-3
	 */
	private int r;
	/**
	 * Dlguosc slowa w funkcji skrotu SHA-3
	 */
	private int w;
	/**
	 * Przeplywnosc funkcji rudnowej SHA-3
	 */
	private int c;
	/**
	 * Permutacja funkcji rundowej SHA-3
	 */
	private int b;
	/**
	 * Liczba rund wykonania sie funkcji rundowej
	 */
	private int n;

	/**
	 * Konstruktor funkcji
	 * 
	 * @param b
	 *            permutacja funkcji rundowej SHA-3
	 * @param r
	 *            dlugosc bloku funkcji SHA-3
	 * @param c
	 *            przeplywnosc funkcji SHA-3
	 */
	public Keccak(int b, int r, int c) {

		if (b != r + c) {
			this.b = 1600;
			this.c = 1024;
			this.r = 576;
		} else {
			this.b = b;
			this.c = c;
			this.r = r;
		}

		S = new BigInteger[5][5];
		w = this.b / 25;
		n = (int) (12 + 2 * (Math.log(w) / Math.log(2)));

	}

	/**
	 * Funckja odpowiadajaca za stworzenie skrotu
	 * 
	 * @param message
	 *            wiadomosc do tworzenia sktoru
	 * @return skrot wiadomosci
	 */
	public String getHash(String message) {

		// padding
		String paddedMessage = KeccakUtils.padding10star1(message, r);
		ArrayList<ArrayList<BigInteger>> blocksMessage = KeccakUtils.getMessageBlocks(paddedMessage, r, w);

		// initialization
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				S[i][j] = new BigInteger("0");
			}
		}

		// copy array for PresentationData
		BigInteger[][] stateZero = new BigInteger[5][5];
		stateZero = PresentationData.copyArray(S, stateZero);
		boolean presentationData = true;

		// absorbing
		for (ArrayList<BigInteger> Pi : blocksMessage) {
			for (int i = 0; i < 5; i++) {
				for (int j = 0; j < 5; j++) {
					if ((i + j * 5) < (r / w)) {
						S[i][j] = S[i][j].xor(Pi.get(i + 5 * j));
					}
				}
			}

			// adding data to PresentationData class
			if (presentationData) {
				BigInteger[][] stateAfterAbsorbcion = new BigInteger[5][5];
				stateAfterAbsorbcion = PresentationData.copyArray(S, stateAfterAbsorbcion);
				presentationData = false;
				PresentationData.setAbsorbcionData(stateZero, Pi, stateAfterAbsorbcion);
			}

			keccakRoundFunction();
		}

		// squizing
		String Z = "";

		do {
			for (int i = 0; i < 5; i++) {
				for (int j = 0; j < 5; j++) {
					if ((5 * i + j) < (r / w)) {
						Z = Z + KeccakUtils.reverseHex(KeccakUtils.bigIntegerToHex(S[j][i]));
						if (PresentationData.squizesToMake > 0) {
							PresentationData.squizesArray.add(Z);
							PresentationData.squizesStateArray.add(KeccakUtils.reverseHex(KeccakUtils
									.bigIntegerToHex(S[j][i])));
							PresentationData.squizesToMake--;
						} else {
							PresentationData.setDataSquize();
						}
					}
				}
			}
			keccakRoundFunction();
		} while (Z.length() < c / 8);
		return Z.substring(0, c / 8);
	}

	/**
	 * Funkcja rundowa SHA-3
	 */
	public void keccakRoundFunction() {

		for (int round = 0; round < n; round++) {

			B = new BigInteger[5][5];
			C = new BigInteger[5];
			D = new BigInteger[5];

			for (int i = 0; i < 5; i++) {
				C[i] = BigInteger.ZERO;
				D[i] = BigInteger.ZERO;
				for (int j = 0; j < 5; j++) {
					B[i][j] = BigInteger.ZERO;
				}
			}

			theta();
			ro_pi();
			chi();
			iota(round);
		}
	}

	/**
	 * Krok theta funkcji rundowej
	 */
	private void theta() {

		if (PresentationData.setDataThetaStep) {
			PresentationData.beforeTheta.add(PresentationData.stateToString("S", S));
			PresentationData.beforeTheta.add(PresentationData.stateToString("C", C));
			PresentationData.beforeTheta.add(PresentationData.stateToString("D", D));

		}

		for (int x = 0; x < 5; x++) {
			C[x] = S[x][0].xor(S[x][1]).xor(S[x][2]).xor(S[x][3]).xor(S[x][4]);

		}
		for (int x = 0; x < 5; x++) {
			D[x] = C[(x + 4) % 5].xor(KeccakUtils.rotateLeft(C[(x + 1) % 5], 1));
		}
		for (int x = 0; x < 5; x++) {
			for (int y = 0; y < 5; y++) {
				S[x][y] = S[x][y].xor(D[x]);
			}
		}

		if (PresentationData.setDataThetaStep) {

			PresentationData.afterTheta.add(PresentationData.stateToString("S", S));
			PresentationData.afterTheta.add(PresentationData.stateToString("C", C));
			PresentationData.afterTheta.add(PresentationData.stateToString("D", D));
			PresentationData.setDataThetaStep = false;
			PresentationData.setDataTheta();
		}

	}

	/**
	 * Kroki Rho, Pi funkcji rundowej
	 */
	private void ro_pi() {

		if (PresentationData.setDataRhoPiStep) {
			PresentationData.beforeRhoPi = PresentationData.copyArray(B, PresentationData.beforeRhoPi);
		}

		for (int x = 0; x < 5; x++) {
			for (int y = 0; y < 5; y++) {
				B[y][(2 * x + 3 * y) % 5] = KeccakUtils.rotateLeft(S[x][y], Offset.getOffset(x, y));
			}
		}

		if (PresentationData.setDataRhoPiStep) {
			PresentationData.afterRhoPi = PresentationData.copyArray(S, PresentationData.afterRhoPi);
			PresentationData.setDataRhoPiStep = false;
			PresentationData.setDataRhoPi();
		}
	}

	/**
	 * Krok Chi funkcji rundowej
	 */
	private void chi() {
		if (PresentationData.setDataChiStep) {
			PresentationData.beforeChi.add(PresentationData.stateToString("S", S));
			PresentationData.beforeChi.add(PresentationData.stateToString("C", C));
		}

		for (int x = 0; x < 5; x++) {
			for (int y = 0; y < 5; y++) {
				S[x][y] = B[x][y].xor((B[(x + 1) % 5][y].not()).and(B[(x + 2) % 5][y]));
			}
		}

		if (PresentationData.setDataChiStep) {
			PresentationData.afterChi.add(PresentationData.stateToString("S", S));
			PresentationData.setDataChiStep = false;
			PresentationData.setDataChi();
		}
	}

	/**
	 * Krok iota funkcji rundowej
	 * 
	 * @param n
	 *            numer stalej RC uzywanej w kroku iota
	 */
	private void iota(int n) {
		if (PresentationData.setDataIotaStep) {
			PresentationData.beforeIota.add(PresentationData.stateToString("S", S));
			PresentationData.beforeIota.add("RC[" + n + "]" + " = " + RC.getConstant(n));
		}

		S[0][0] = S[0][0].xor(RC.getConstant(n));

		if (PresentationData.setDataIotaStep) {
			PresentationData.afterIota = PresentationData.copyArray(S, PresentationData.afterIota);
			PresentationData.setDataIotaStep = false;
			PresentationData.setDataIota();
		}
	}

}
