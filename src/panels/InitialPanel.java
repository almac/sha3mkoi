package panels;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Klasa sluzaca do podania danych wejsciowych uzywanych przez funkcje skrotu
 * SHA3
 * 
 * @author Maciej Aleszczyk
 * @author Grzegorz Dziubka
 */
public class InitialPanel extends JPanel {
	/**
	 * Pole tekstowe sluzace do wpisania danych wejsciowych uzywanych przez
	 * funkcje skrotu SHA3
	 */
	private JTextField inputMessageTextField;

	/**
	 * Konstruktor klasy InitialPanel
	 */
	public InitialPanel() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		JLabel textInputLabel = new JLabel("Dane wejsciowe funkcji skrotu SHA3");
		GridBagConstraints gbc_textInputLabel = new GridBagConstraints();
		gbc_textInputLabel.insets = new Insets(0, 0, 5, 0);
		gbc_textInputLabel.gridx = 0;
		gbc_textInputLabel.gridy = 0;
		add(textInputLabel, gbc_textInputLabel);

		inputMessageTextField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 0;
		gbc_textField.gridy = 1;
		add(inputMessageTextField, gbc_textField);
		inputMessageTextField.setColumns(10);

	}

	/**
	 * Funkcja sluzaca do pobrania danych wejsciowych z panelu InitialPanel
	 * 
	 * @return dane wejsciowe uzywane przez funkcje skrotu SHA3
	 */
	public String getInputMessage() {
		return inputMessageTextField.getText();
	}

}
