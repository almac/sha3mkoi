package panels;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Dwuskladnikowy generyczny panel odpowiadajacy za pokazanie danych dotyczacych
 * stanu funkcji krotu przed i po wykonaniu kroku, a takze pokazanie opisu
 * dotyczacego tego kroku
 * 
 * @author Maciej Aleszczyk
 * @author Grzegorz Dziubka
 */
public class GenericRoundFunctionPanel extends JPanel {

	/**
	 * Generyczny panel sluzacy do pokazania opisu pokazywanego kroku
	 */
	private GenericRoundFunctionDescriptionPanel descriptionPanel;
	/**
	 * Generyczny panel sluzacy do pokazania danych stanu funkcji skrotu dla
	 * prezentowanego kroku
	 */
	private GenericRoundFunctionDataPanel dataPanel;

	/**
	 * Kontruktor klasy GenericRoundFunctionPanel
	 * 
	 * @param title
	 *            nazwa prezentowanego etapu
	 * @param description
	 *            opis prezentowanego kroku
	 * @param url
	 *            sciezka obrazu ilustrujacego dany krok
	 * @param beforeStepData
	 *            dane przed wykonianiem kroku
	 * @param afterStepData
	 *            dane po wykonaniu kroku
	 */
	public GenericRoundFunctionPanel(String title, String description, String url, String beforeStepData,
			String afterStepData) {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 600, 600, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		JLabel stepName = new JLabel(title);
		GridBagConstraints gbc_stepName = new GridBagConstraints();
		gbc_stepName.gridwidth = 2;
		gbc_stepName.insets = new Insets(0, 0, 5, 5);
		gbc_stepName.gridx = 0;
		gbc_stepName.gridy = 0;
		add(stepName, gbc_stepName);

		dataPanel = new GenericRoundFunctionDataPanel(beforeStepData, afterStepData);
		GridBagConstraints gbc_dataPanel = new GridBagConstraints();
		gbc_dataPanel.fill = GridBagConstraints.BOTH;
		gbc_dataPanel.insets = new Insets(0, 0, 0, 5);
		gbc_dataPanel.gridx = 0;
		gbc_dataPanel.gridy = 1;
		add(dataPanel, gbc_dataPanel);

		descriptionPanel = new GenericRoundFunctionDescriptionPanel(description, url);
		GridBagConstraints gbc_infoPanel = new GridBagConstraints();
		gbc_infoPanel.fill = GridBagConstraints.BOTH;
		gbc_infoPanel.gridx = 1;
		gbc_infoPanel.gridy = 1;
		add(descriptionPanel, gbc_infoPanel);

	}

}
