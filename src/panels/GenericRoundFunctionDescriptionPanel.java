package panels;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Generyczny panel sluzacy do prezentacji opisu aktualnego kroku funkcji skrotu
 * SHA3
 * 
 * @author Maciej Aleszczyk
 * @author Grzegorz Dziubka
 */
public class GenericRoundFunctionDescriptionPanel extends JPanel {

	/**
	 * Konstruktor klasy GenericRoundFunctionDescriptionPanel
	 * 
	 * @param description
	 *            opis wyswietlany w panelu
	 * @param url
	 *            sciezka do pliku graficznego wyswietlanego w panelu
	 */
	public GenericRoundFunctionDescriptionPanel(String description, String url) {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		JLabel labelStepInfo = new JLabel("Opis kroku");
		GridBagConstraints gbc_labelStepInfo = new GridBagConstraints();
		gbc_labelStepInfo.insets = new Insets(0, 0, 5, 0);
		gbc_labelStepInfo.gridx = 0;
		gbc_labelStepInfo.gridy = 0;
		add(labelStepInfo, gbc_labelStepInfo);

		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 1;
		add(scrollPane, gbc_scrollPane);

		JPanel panel = new JPanel();
		scrollPane.setViewportView(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 0, 0 };
		gbl_panel.rowHeights = new int[] { 0, 0, 0 };
		gbl_panel.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		if (url != null) {
			BufferedImage stepPics = null;
			try {
				stepPics = ImageIO.read(this.getClass().getResource(url));
			} catch (IOException e) {
				e.printStackTrace();
			}
			JLabel stepPicture = new JLabel(new ImageIcon(stepPics));
			GridBagConstraints gbc_stepPicture = new GridBagConstraints();
			gbc_stepPicture.fill = GridBagConstraints.BOTH;
			gbc_stepPicture.gridx = 0;
			gbc_stepPicture.gridx = 0;
			gbc_stepPicture.gridy = 0;
			panel.add(stepPicture, gbc_stepPicture);
		}

		JTextArea textPaneStepDecription = new JTextArea();
		textPaneStepDecription.setEditable(false);
		textPaneStepDecription.setLineWrap(true);
		textPaneStepDecription.setWrapStyleWord(true);
		textPaneStepDecription.setText(description);
		GridBagConstraints gbc_textPaneStepDecription = new GridBagConstraints();
		gbc_textPaneStepDecription.fill = GridBagConstraints.BOTH;
		gbc_textPaneStepDecription.gridx = 0;
		gbc_textPaneStepDecription.gridy = 1;
		panel.add(textPaneStepDecription, gbc_textPaneStepDecription);

	}

}
