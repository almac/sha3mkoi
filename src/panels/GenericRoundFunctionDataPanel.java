package panels;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Generyczny panel sluzacy do prezentacji danych stanu funkcji skrotu przed i
 * po wykonaniu kroku
 * 
 * @author Maciej Aleszczyk
 * @author Grzegorz Dziubka
 */
public class GenericRoundFunctionDataPanel extends JPanel {

	/**
	 * Pole tekstowe sluzace do prezentacji danych stanu funkcji skrotu przed
	 * wykonaniem kroku
	 */
	private JTextArea textAreaDataBeforeStep;
	/**
	 * Pole tekstowe sluzace do prezentacji danych stanu funkcji skrotu po
	 * wykonaniem kroku
	 */
	private JTextArea textAreaDataAfterStep;

	/**
	 * Dane dotyczace stanu sprzed wykonania kroku
	 */
	private String beforeStepData;
	/**
	 * Dane dotyczace stanu po wykonaniu kroku
	 */
	private String afterStepData;

	/**
	 * Konstruktor klasy GenericRoundFunctionDataPanel
	 */
	public GenericRoundFunctionDataPanel(String beforeStepData, String afterStepData) {
		this.beforeStepData = beforeStepData;
		this.afterStepData = afterStepData;

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 294, 299, 0 };
		gridBagLayout.rowHeights = new int[] { 14, 14, 434, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		JLabel labelData = new JLabel("Dane stanu dla przykladowej iteracji z opisywanego kroku");
		GridBagConstraints gbc_labelData = new GridBagConstraints();
		gbc_labelData.insets = new Insets(0, 0, 5, 0);
		gbc_labelData.gridwidth = 2;
		gbc_labelData.gridx = 0;
		gbc_labelData.gridy = 0;
		add(labelData, gbc_labelData);

		JLabel labelDataBeforeStep = new JLabel("Stan przed wykonaniem kroku");
		GridBagConstraints gbc_labelDataBeforeStep = new GridBagConstraints();
		gbc_labelDataBeforeStep.insets = new Insets(0, 0, 5, 5);
		gbc_labelDataBeforeStep.gridx = 0;
		gbc_labelDataBeforeStep.gridy = 1;
		add(labelDataBeforeStep, gbc_labelDataBeforeStep);

		JLabel labelDataAfterStep = new JLabel("Stan po wykonaniu kroku");
		GridBagConstraints gbc_labelDataAfterStep = new GridBagConstraints();
		gbc_labelDataAfterStep.insets = new Insets(0, 0, 5, 0);
		gbc_labelDataAfterStep.gridx = 1;
		gbc_labelDataAfterStep.gridy = 1;
		add(labelDataAfterStep, gbc_labelDataAfterStep);

		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.insets = new Insets(0, 0, 0, 5);
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 2;
		add(scrollPane, gbc_scrollPane);

		JPanel panel = new JPanel();
		scrollPane.setViewportView(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 0, 0 };
		gbl_panel.rowHeights = new int[] { 0, 0 };
		gbl_panel.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		textAreaDataBeforeStep = new JTextArea();
		GridBagConstraints gbc_textAreaDataBeforeStep = new GridBagConstraints();
		gbc_textAreaDataBeforeStep.fill = GridBagConstraints.BOTH;
		gbc_textAreaDataBeforeStep.gridx = 0;
		gbc_textAreaDataBeforeStep.gridy = 0;
		panel.add(textAreaDataBeforeStep, gbc_textAreaDataBeforeStep);

		JScrollPane scrollPane_1 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_1.gridx = 1;
		gbc_scrollPane_1.gridy = 2;
		add(scrollPane_1, gbc_scrollPane_1);

		JPanel panel_1 = new JPanel();
		scrollPane_1.setViewportView(panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] { 0, 0 };
		gbl_panel_1.rowHeights = new int[] { 0, 0 };
		gbl_panel_1.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_1.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		panel_1.setLayout(gbl_panel_1);

		textAreaDataAfterStep = new JTextArea();
		GridBagConstraints gbc_textAreaDataAfterStep = new GridBagConstraints();
		gbc_textAreaDataAfterStep.fill = GridBagConstraints.BOTH;
		gbc_textAreaDataAfterStep.gridx = 0;
		gbc_textAreaDataAfterStep.gridy = 0;
		panel_1.add(textAreaDataAfterStep, gbc_textAreaDataAfterStep);

		setTextAreaDataBeforeStep(beforeStepData);
		setTextAreaDataAfterStep(afterStepData);

	}

	/**
	 * Funkcja sluzaca do wpisania danych stanu funkcji skrotu przed wykonaniem
	 * kroku
	 * 
	 * @param dataBefore
	 *            stan przed wykonianiem krotku
	 */
	private void setTextAreaDataBeforeStep(String dataBefore) {
		this.textAreaDataBeforeStep.setText(dataBefore);
	}

	/**
	 * Funkcja sluzaca do wpisania danych stanu funkcji skrotu po wykonaniem
	 * kroku
	 * 
	 * @param dataAfter
	 *            stan po wykonaniu kroku
	 */
	private void setTextAreaDataAfterStep(String dataAfter) {
		this.textAreaDataAfterStep.setText(dataAfter);
	}
}
