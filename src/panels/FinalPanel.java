package panels;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Panel sluzacy do wyswietlania wynikow pracy funkcji skrotu SHA3
 * 
 * @author Maciej Aleszczyk
 * @author Grzegorz Dziubka
 */
public class FinalPanel extends JPanel {

	/**
	 * Konstruktor klasy FinalPanel, ktora sluzy do wyswietlania wynikow pracy
	 * funkcji skrotu SHA3
	 * 
	 * @param input
	 *            dane wejsciowe funkcji skrotu SHA3
	 * @param output
	 *            dane wynikowe funkcji skrotu SHA3
	 */
	public FinalPanel(String input, String output) {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		JLabel labelInputSHA3 = new JLabel("Wartosc wejsciowa funkcji skrótu");
		GridBagConstraints gbc_labelInputSHA3 = new GridBagConstraints();
		gbc_labelInputSHA3.insets = new Insets(0, 0, 5, 0);
		gbc_labelInputSHA3.gridx = 0;
		gbc_labelInputSHA3.gridy = 0;
		add(labelInputSHA3, gbc_labelInputSHA3);

		JTextField inputSHA3 = new JTextField();
		GridBagConstraints gbc_inputSHA3 = new GridBagConstraints();
		gbc_inputSHA3.fill = GridBagConstraints.HORIZONTAL;
		gbc_inputSHA3.insets = new Insets(0, 0, 5, 0);
		gbc_inputSHA3.gridx = 0;
		gbc_inputSHA3.gridy = 1;
		add(inputSHA3, gbc_inputSHA3);

		JLabel labelOutputSHA3 = new JLabel("Wartosc wyjsciowa funkcji skrótu");
		GridBagConstraints gbc_labelOutputSHA3 = new GridBagConstraints();
		gbc_labelOutputSHA3.insets = new Insets(0, 0, 5, 0);
		gbc_labelOutputSHA3.gridx = 0;
		gbc_labelOutputSHA3.gridy = 3;
		add(labelOutputSHA3, gbc_labelOutputSHA3);

		JTextField outputSHA3 = new JTextField();
		GridBagConstraints gbc_outputSHA3 = new GridBagConstraints();
		gbc_outputSHA3.fill = GridBagConstraints.HORIZONTAL;
		gbc_outputSHA3.gridx = 0;
		gbc_outputSHA3.gridy = 4;
		add(outputSHA3, gbc_outputSHA3);

		inputSHA3.setText(input);
		outputSHA3.setText(output);

	}

}
