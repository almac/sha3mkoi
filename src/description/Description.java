package description;

/**
 * Klasa odpowiedzialna za przechowywanie opis�w zwiazanych z poszczeg�lnymi
 * etapami pracy funkcji skr�tu SHA3
 * 
 * @author Maciej Aleszczyk
 * @author Grzegorz Dziubka
 */
public class Description {

	/**
	 * Opis etapu inicjalizacji i uzupelniania
	 */
	public static final String INITIALISATION = "W tym etapie tworzony jest stan pocz�tkowy S[x,y]. Na samym pocz�tku jest on uzupe�niany zerami. Nast�pnie wiadomo��, kt�rej skr�t ma zosta� obliczony jest uzupe�niany na podstawie regu�y pad 10*1. Polega ona na uzupe�nieniu wiadomo�ci bajtem 0x01 oraz bajtami 0x00 tak, aby sumaryczna d�ugo�� by�a r�wna wielokrotno�ci d�ugo�ci blok�w r. Potem wykonywana jest operacja XOR na uzupe�nionej wiadomo�ci P i ci�gu bajt�w 0x00 || � || 0x00 || 0x80, o tej samej d�ugo�ci co P.";
	/**
	 * Opis etapu absorbcji
	 */
	public static final String ABSORBTION = "Faza ta polega na aktualizacji warto�ci toru stanu S[x,y] przez wykonanie na nim operacji XOR z odpowiednim blokiem wiadomo�ci P. Tak zaktualizowany stan S jest zast�powany warto�ci� wynikow� funkcji rundowej wywo�anej na tym stanie. Operacje te wykonywane s� na ka�dym z blok�w Pi.";
	/**
	 * Opis kroku Theta wystepujacego w funkcji rundowej
	 */
	public static final String THETA = "Rysunek zawiera schematyczn� reprezentacj� operacji Theta. Pokazuje on, �e oczekiwanym rezultatem operacji jest uzyskanie sumy jednej kolumny, uzyskanie sumy drugiej kolumny oraz dodanie tych sum do jednego bitu. Pierwsza cz�� pseudokodu powoduje pobranie ka�dej warstwy (ang. sheet) i wykonanie na nich operacji XOR, po czym nast�puje utworzenie toru, zapisanego w tablicy C. W kolejnym kroku ponownie nast�puje XOR, tym razem na torach. Do tego celu nale�y pobra� odpowiednie tory, dlatego wykorzystuje si� rotacj�. Wynik zapisywany jest w tablicy D, u�ywanej w ostatnim etapie operacji Theta. ";
	/**
	 * Opis kroku Rho wystepujacego w funkcji rundowej
	 */
	public static final String RHO = "Do fazy Rho wykorzystywane s� sta�e offsetowe. Zgodnie z ich warto�ci� nast�puje odpowiednia rotacja ka�dego toru wewn�trz stanu. Wynikiem jest przestawiona kolejno�� w ci�gu bit�w toru. Etap Rho wykonywany jest jednoczesnie z etapem Pi.";
	/**
	 * Opis kroku Pi wystepujacego w funkcji rundowej
	 */
	public static final String PI = "Wynikiem jest przestawiona kolejno�� w ci�gu bit�w toru. W kroku Pi nast�puje odpowiednia rotacja tor�w wewn�trz stan�w, a nie bity jak w przypadku operacji Rho. Operacje Rho i Pi s�u�� do rozpraszania bit�w. Etap ten wykonywany jest jednoczesnie z etapem Rho.";
	/**
	 * Opis kroku Chi wystepujacego w funkcji rundowej
	 */
	public static final String CHI = "Etap Chi jest stosunkowo prostym etapem, w kt�rym wykorzystywane s� operacje bitowe AND, NOT i XOR. Powy�szy rysunek prezentuje owe operacje w powi�zaniu z w�a�ciwymi wierszami stanu. Je�li we�miemy element (s�owo) A, to nale�y nast�pnie wzi�� kolejny po nim element Y (o jeden w prawo od A) i Z (o dwa w prawo od A). Wtedy r�wnanie mo�na zapisa� jako A XOR ((NOT Y)  AND Z)";
	/**
	 * Opis kroku Iota wystepujacego w funkcji rundowej
	 */
	public static final String IOTA = "Krok Iota jest ostatnim etapem funkcji rundowej, kt�ry opiera si� na dodawaniu sta�ej rundowej RC. Celem tej fazy jest zaburzenie symetrii, aby zabezpieczy� funkcj�, a co za tym idzie ca�y algorytm, przed atakami wykorzystuj�cymi symetri�.";
	/**
	 * Opis etapu wyciskania
	 */
	public static final String SQUEEZING = "Zadaniem tej fazy jest na samym pocz�tku stworzenie pustego wynikowego ci�gu znak�w Z. Nast�pnie poprzez iteracyjne pobieranie r pierwszych bit�w ze stanu S i konkatenacj� z ci�giem Z tworzony jest ci�g wynikowy. Po ka�dym pobraniu bit�w, stan jest aktualizowany poprzez wywo�anie na nim funkcji rundowej. ";
	/**
	 * Opis prezentacji danych
	 */
	public static final String FINALISATION = "";

}
