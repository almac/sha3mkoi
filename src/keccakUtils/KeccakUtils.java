package keccakUtils;

import java.math.BigInteger;
import java.util.ArrayList;

import presentationData.PresentationData;

/**
 * Klasa narzedzi wspomagajacych prace funkcji skrotu SHA-3
 * 
 * @author Maciej Aleszczyk
 * @author Grzegorz Dziubka
 * 
 */
public class KeccakUtils {

	/**
	 * Funkcja dzielaca wiadomosc na bloki Pi o dlugosci r i skladajacych sie ze
	 * slow w-bitowych
	 * 
	 * @param message
	 *            wiadomosc do podzielenia
	 * @param r
	 *            dlugosc bitowa bloku wiadomosci
	 * @param w
	 *            dlugosci bitowa slowa
	 * @return lista blokow podzielonych na slowa w-bitowe
	 */
	public static ArrayList<ArrayList<BigInteger>> getMessageBlocks(String message, int r, int w) {

		ArrayList<ArrayList<BigInteger>> messageBlocks = new ArrayList<ArrayList<BigInteger>>();

		int piBlockNumber = 0;
		int wordNumber = 0;
		messageBlocks.add(new ArrayList<BigInteger>());
		int counter = 0;
		for (int i = 0; i < message.length(); i++) {

			if (wordNumber > (r / w - 1)) {
				wordNumber = 0;
				piBlockNumber++;
				messageBlocks.add(new ArrayList<BigInteger>());
			}
			counter++;
			if ((counter * 4 % w) == 0) {
				String wordHex = message.substring(counter - w / 4, counter);
				String reversedWordHex = KeccakUtils.reverseHex(wordHex);
				BigInteger word = KeccakUtils.hexToBigInteger(reversedWordHex);
				messageBlocks.get(piBlockNumber).add(word);

				wordNumber++;
			}

		}
		return messageBlocks;

	}

	/**
	 * Funkcja tworzaca zmienna BigInteger o L jedynkach
	 * 
	 * @param L
	 *            liczba jedynek
	 * @return zmienna BigInteger
	 */
	public static BigInteger allOnes(int L) {
		return BigInteger.ZERO.setBit(L).subtract(BigInteger.ONE);
	}

	/**
	 * Funkcja dokonujaca bitowego cyklicznego przesuniecia slowa w lewo
	 * 
	 * @param n
	 *            slowo podane do przesuniecia w lewo
	 * @param k
	 *            liczba bitow o ktore nalezy przesunac slowo
	 * @return slowo przesuniete bitowo o k bitow
	 */
	public static BigInteger rotateLeft(BigInteger n, int k) {
		return n.shiftLeft(k).or(n.shiftRight(64 - k)).and(allOnes(64));
	}

	/**
	 * Funkcja zamieniajaca ciag znakow na ciag znakow sformatowane w systemie
	 * 16stkowym
	 * 
	 * @param message
	 *            wiadomosc do transformacji
	 * @return wiadomosc w notacji heksadecymalnej
	 */
	public static String toHex(String message) {
		if (message.equals("")) {
			return "";
		}
		return String.format("%x", new BigInteger(1, message.getBytes()));
	}

	/**
	 * Funkcja zamieniajaca zmienna BigInteger na ciag znakow w notacji
	 * heksadecymalnej
	 * 
	 * @param l
	 *            zmienna poddana transformacji
	 * @return ciag znakow zapisany w notacji heksadecymalnej
	 */
	public static String bigIntegerToHex(BigInteger l) {
		return l.toString(16);
	}

	/**
	 * Funkcja zmieniajaca ciag znakow zapisanych w systemie heksadecymalnym w
	 * zmienna BigInteger
	 * 
	 * @param hex
	 *            ciag znakow poddawany zmianie
	 * @return zmienna BigInteger
	 */
	public static BigInteger hexToBigInteger(String hex) {
		return new BigInteger(hex, 16);
	}

	/**
	 * Funkcja dokonujaca odwrocenia ciagu znakow zapisanych w systemie
	 * heksadecymalnym
	 * 
	 * @param hex
	 *            ciag znakow do odwrocenia
	 * @return odwrocony ciag
	 */
	public static String reverseHex(String hex) {

		StringBuilder sb = new StringBuilder();
		if (hex.length() % 2 != 0) {
			hex = "0" + hex;
		}
		for (int i = hex.length(); i > 1; i -= 2)
			sb.append(hex.substring(i - 2, i));

		return sb.toString();
	}

	/**
	 * Funkcja dokonujaca uzupelniana wiadomosci wg systemu 10*1
	 * 
	 * @param message
	 *            wiadomosc do uzupelnienia bitowego
	 * @param block
	 *            dlugosc bloku wiadomosci
	 * @return uzupelniona wiadomosc
	 */
	public static String padding10star1(String message, int block) {

		StringBuilder sb = new StringBuilder();
		String messageHexed = toHex(message);

		int messageLenght = messageHexed.length() * 4;

		int mod = (messageLenght) % block;
		int toAppend = (block - mod) / 8;

		sb.append(messageHexed);

		// System.out.println("block: " + block);
		// System.out.println("message lenght: " + messageLenght);
		// System.out.println("toAppend: " + toAppend);

		if (toAppend == 2) {
			sb.append("81");
		} else {
			sb.append("01");
			for (int i = toAppend - 1; i > 1; i--) {
				sb.append("00");
			}
			sb.append("80");
		}

		PresentationData.setInitialisationData(messageHexed, sb.toString());

		return sb.toString();
	}

}
