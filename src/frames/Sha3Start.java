package frames;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import keccak.Keccak;
import panels.FinalPanel;
import panels.GenericRoundFunctionPanel;
import panels.InitialPanel;
import presentationData.PresentationData;
import description.Description;

/**
 * Klasa glowna programu, odpowiedzialna jest za prezentacje dzialania funkcji
 * skrotu SHA3
 * 
 * @author Maciej Aleszczyk
 * @author Grzegorz Dziubka
 */
public class Sha3Start extends JFrame {

	/**
	 * Panel, w kt�rym przechowywane s� panele reprezentujace poszczegolne kroki
	 * dzialania funkcji skrotu SHA3
	 */
	private JPanel contentPane;
	/**
	 * Panel przechowujacy referencje do ostatnio dodanego panelu
	 */
	private JPanel previousPanel;
	/**
	 * Panel poczatkowy
	 */
	private InitialPanel initialPanel;
	/**
	 * Obiekt przechowujacy informacje o polozeniu elementow w panelu
	 */
	private GridBagConstraints gbc_panel;
	/**
	 * Przycisk przechodzenia do kolejnego kroku pracy funkcji skrotu SHA3
	 */
	private JButton nextButton;
	/**
	 * Obiekt przechowujacy informacje o numerze aktualnego kroku
	 */
	private int currentStep = 1;

	private Keccak keccak;

	/**
	 * Funkcja glowna odpowiedzialna za uruchamianie aplikacji
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Sha3Start frame = new Sha3Start();
					frame.setExtendedState(Frame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Funkcja odpowiedzialna za zmiane akutalnie wyswietlanego panelu w glownym
	 * oknie aplikacji
	 */
	private void showNextPanel() {
		GenericRoundFunctionPanel stepPanel = null;

		if (currentStep == 2) {
			stepPanel = new GenericRoundFunctionPanel("Etap inicjalizacji i dopeniania", Description.INITIALISATION,
					null, PresentationData.messageInit, PresentationData.messageAfterPadding);
			this.setTitle("SHA 3 - Etap inicjalizacji i dopeniania");

		} else if (currentStep == 3) {
			stepPanel = new GenericRoundFunctionPanel("Etap absorbcji", Description.ABSORBTION, null,
					PresentationData.stateAfterAbsorbcion, PresentationData.stateBeforeAbsorbcion);
			this.setTitle("SHA 3 - Etap absorbcji");

		} else if (currentStep == 4) {
			stepPanel = new GenericRoundFunctionPanel("Funkcja rundowa - etap Theta", Description.THETA,
					"/graphics/theta.png", PresentationData.stateBeforeTheta, PresentationData.stateAfterTheta);
			this.setTitle("SHA 3 - Funkcja rundowa - etap Theta");

		} else if (currentStep == 5) {
			stepPanel = new GenericRoundFunctionPanel("Funkcja rundowa - etap Rho", Description.RHO,
					"/graphics/rho2.png", PresentationData.stateBeforeRhoPi, PresentationData.stateAfterRhoPi);
			this.setTitle("SHA 3 - Funkcja rundowa - etap Rho");

		} else if (currentStep == 6) {
			stepPanel = new GenericRoundFunctionPanel("Funkcja rundowa - etap Pi", Description.PI, "/graphics/pi.png",
					PresentationData.stateBeforeRhoPi, PresentationData.stateAfterRhoPi);
			this.setTitle("SHA 3 - Funkcja rundowa - etap Pi");

		} else if (currentStep == 7) {
			stepPanel = new GenericRoundFunctionPanel("Funkcja rundowa - etap Chi", Description.CHI,
					"/graphics/chi.png", PresentationData.stateBeforeChi, PresentationData.stateAfterChi);
			this.setTitle("SHA 3 - Funkcja rundowa - etap Chi");

		} else if (currentStep == 8) {
			stepPanel = new GenericRoundFunctionPanel("Funkcja rundowa - etap Iota", Description.IOTA, null,
					PresentationData.stateBeforeIota, PresentationData.stateAfterIota);
			this.setTitle("SHA 3 -Funkcja rundowa - etap Iota");

		} else if (currentStep == 9) {
			stepPanel = new GenericRoundFunctionPanel("Etap wyciskania", Description.SQUEEZING, null,
					PresentationData.hashInitial, PresentationData.hashAfterSquize);
			this.setTitle("SHA 3 - Etap wyciskania");

		} else if (currentStep == 10) {
			this.setTitle("SHA 3 - Wyniki funkcji skr�tu");
			FinalPanel panel = new FinalPanel(PresentationData.message, PresentationData.hashedMessage);
			nextButton.setText("Oblicz ponownie skr�t wiadomosci");
			contentPane.remove(previousPanel);
			contentPane.add(panel, gbc_panel);
			contentPane.revalidate();
			previousPanel = panel;
			currentStep = 0;

		}

		if (stepPanel != null) {
			contentPane.remove(previousPanel);
			previousPanel = stepPanel;
			contentPane.add(stepPanel, gbc_panel);
			contentPane.revalidate();
		}

	}

	/**
	 * Konstruktor klasy Sha3Start
	 */
	public Sha3Start() {
		setTitle("SHA 3");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 629, 510);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 427, 32, 0 };
		gbl_contentPane.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 1.0, 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		initialPanel = new InitialPanel();
		gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		contentPane.add(initialPanel, gbc_panel);
		previousPanel = initialPanel;

		JPanel buttonPanel = new JPanel();
		GridBagConstraints gbc_buttonPanel = new GridBagConstraints();
		gbc_buttonPanel.fill = GridBagConstraints.BOTH;
		gbc_buttonPanel.gridx = 0;
		gbc_buttonPanel.gridy = 1;
		contentPane.add(buttonPanel, gbc_buttonPanel);
		buttonPanel.setLayout(new BorderLayout(0, 0));

		nextButton = new JButton("Nastepny krok");
		buttonPanel.add(nextButton);
		nextButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				if (currentStep == 0) {
					setTitle("SHA 3");

					PresentationData.resetData();
					initialPanel = new InitialPanel();

					nextButton.setText("Nastepny krok");
					contentPane.remove(previousPanel);
					previousPanel = initialPanel;
					contentPane.add(initialPanel, gbc_panel);
					contentPane.revalidate();
				}

				if (currentStep == 1) {
					PresentationData.message = initialPanel.getInputMessage();
					keccak = new Keccak(1600, 576, 1024);
					PresentationData.hashedMessage = keccak.getHash(PresentationData.message);

				}
				currentStep++;
				showNextPanel();
			}
		});
	}

}
