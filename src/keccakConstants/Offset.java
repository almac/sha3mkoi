package keccakConstants;

/**
 * Klasa odpowiedzialna za przechowywanie stalych offsetowych uzywanych podczas
 * pracy funkjci skrotu SHA3
 * 
 * @author Maciej Aleszczyk
 * @author Grzegorz Dziubka
 */
public class Offset {

	/**
	 * Dwuwymiarowa tablica przechowujaca stale offsetowe
	 */
	private static int[][] offsetMap = new int[][] { { 0, 36, 3, 41, 18 }, { 1, 44, 10, 45, 2 }, { 62, 6, 43, 15, 61 },
			{ 28, 55, 25, 21, 56 }, { 27, 20, 39, 8, 14 } };

	// { { 0, 1, 62, 28, 27 }, { 36, 44, 6, 55, 20 },
	// { 3, 10, 43, 25, 39 }, { 41, 45, 15, 21, 8 }, { 18, 2, 61, 56, 14 } };

	/**
	 * Funkcja sluzaca do pobierania wartosci stalych offsetowych
	 * 
	 * @param x
	 *            wiersz macierzy
	 * @param y
	 *            kolumna macierzy
	 * @return wartosc stalej offsetowej
	 */
	public static int getOffset(int x, int y) {
		return offsetMap[x][y];
	}
}
