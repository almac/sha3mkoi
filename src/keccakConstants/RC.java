package keccakConstants;

import java.math.BigInteger;
import java.util.HashMap;

/**
 * Klasa enumeracji przechowujaca stale rundowe uzywane w pracy funkcji skrotu SHA3
 * @author Maciej Aleszczyk
 * @author Grzegorz Dziubka
 */
/**
 * @author Maciek
 *
 */
/**
 * @author Maciek
 *
 */
public enum RC {

	/**
	 * Stala rundowa RC 0
	 */
	RC0(0, "0000000000000001"),
	/**
	 * Stala rundowa RC 1
	 */
	RC1(1, "0000000000008082"),
	/**
	 * Stala rundowa RC 2
	 */
	RC2(2, "800000000000808A"),
	/**
	 * Stala rundowa RC 3
	 */
	RC3(3, "8000000080008000"),
	/**
	 * Stala rundowa RC 4
	 */
	RC4(4, "000000000000808B"),
	/**
	 * Stala rundowa RC 5
	 */
	RC5(5, "0000000080000001"),
	/**
	 * Stala rundowa RC 6
	 */
	RC6(6, "8000000080008081"),
	/**
	 * Stala rundowa RC 7
	 */
	RC7(7, "8000000000008009"),
	/**
	 * Stala rundowa RC 8
	 */
	RC8(8, "000000000000008A"),
	/**
	 * Stala rundowa RC 9
	 */
	RC9(9, "0000000000000088"),
	/**
	 * Stala rundowa RC 10 
	 */
	RC10(10, "0000000080008009"),
	/**
	 * Stala rundowa RC 11
	 */
	RC11(11, "000000008000000A"),
	/**
	 * Stala rundowa RC 12
	 */
	RC12(12, "000000008000808B"),
	/**
	 * Stala rundowa RC 13
	 */
	RC13(13, "800000000000008B"),
	/**
	 * Stala rundowa RC 14
	 */
	RC14(14, "8000000000008089"),
	/**
	 * Stala rundowa RC 15
	 */
	RC15(15, "8000000000008003"),
	/**
	 * Stala rundowa RC 16
	 */
	RC16(16, "8000000000008002"),
	/**
	 * Stala rundowa RC 17
	 */
	RC17(17, "8000000000000080"),
	/**
	 * Stala rundowa RC 18
	 */
	RC18(18, "000000000000800A"),
	/**
	 * Stala rundowa RC 19
	 */
	RC19(19, "800000008000000A"),
	/**
	 * Stala rundowa RC 20
	 */
	RC20(20, "8000000080008081"),
	/**
	 * Stala rundowa RC 21
	 */
	RC21(21, "8000000000008080"),
	/**
	 * Stala rundowa RC 22
	 */
	RC22(22, "0000000080000001"),
	/**
	 * Stala rundowa RC 23
	 */
	RC23(23, "8000000080008008");

	/**
	 * wartosc stalej rundowej
	 */
	private String constant;
	/**
	 * numer stalej rundowej
	 */
	private int number;
	/**
	 * Mapa przechowujaca zmienne, uzywana do szybkiego wyciagania wartosci stalych rundowych
	 */
	public static HashMap<Integer, String> map;
	static {
		map = new HashMap<Integer, String>();
		for (RC v : RC.values()) {
			map.put(v.number, v.constant);
		}
	}

	/** Konstruktor enum stalej rundowej
	 * @param number numer stalej rundowej
	 * @param constant wartosc stalej rundowej
	 */
	RC(int number, String constant) {
		this.number = number;
		this.constant = constant;
	}
	
	/**Funkcja sluzaca do pobierania wartosci stalej rundowej
	 * @param number numer stalej rundowej
	 * @return wartosc stalej rundowej
	 */
	public static BigInteger getConstant(int number){
		return new BigInteger(map.get(new Integer(number)),16);
	}

}
