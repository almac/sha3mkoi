var namespaces =
[
    [ "description", "namespacedescription.html", null ],
    [ "frames", "namespaceframes.html", null ],
    [ "keccak", "namespacekeccak.html", null ],
    [ "keccakConstants", "namespacekeccak_constants.html", null ],
    [ "keccakUtils", "namespacekeccak_utils.html", null ],
    [ "panels", "namespacepanels.html", null ],
    [ "presentationData", "namespacepresentation_data.html", null ]
];